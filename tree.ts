#!/usr/bin/env node

"use strict";

var _ = require("lodash");
var rand=require("randomstring");
var sql = require("alasql");

interface IDTree {
    classifier: String;
    features: Array;
    trainingData: Array;
}

interface INode {
    name:String;
    values:Array;
    info:String;
}

class DTree {
    classifier: String;
    features: Array;
    trainingData: Array;
    nodes:[INode];

    constructor(dtree:IDTree) {
        this.classifier=dtree.classifier;
        this.features=dtree.features;
        this.trainingData=dtree.trainingData;
        this.nodes=[];
        this.train();
    }
    private calcGain () {

    }
    private calcEntropy (y:Number,n:Number):Number {
        var total=y+n;
        var bits=(-(y/total)*log2(y/total)-(n/total)*log2(n/total)).toFixed(2);
        if(isNaN(bits))
            return 0;
        return bits;
    }
    private pickBest() {
        this.nodes.forEach(function (node) {
            node.values.forEach(function (value) {

            });
        });
    }
    private createNode (node:INode) {
        this.nodes.push({
            name:node.name,
            values:node.values,
            info:node.info,
        });
    }
    //https://www.youtube.com/watch?v=AmCV4g7_-QM
    private train () {
        var data=this.trainingData;
        var isPure=this.isPure;
        var calcEntropy=this.calcEntropy;
        var features=this.features;
        var self=this;

        features.forEach(function (feature) {
            var values=_.chain(data).map(feature).uniq().value();
            var info=[];
            values.forEach(function (value) {
                var trueCount=sql("select count(*) as c from ? where "+feature+"='"+value+"' and play=true",[data])[0].c;
                var falseCount=sql("select count(*) as c from ? where "+feature+"='"+value+"' and play=false",[data])[0].c;
                //info.push("value: "+value+" has: "+trueCount+" true and: "+falseCount+" false, entropy of: "+calcEntropy(trueCount,falseCount)+" and isPure: "+isPure(trueCount,falseCount));
                info.push({
                    value:value,
                    truthy:trueCount,
                    falsey:falseCount,
                    entropy:calcEntropy(trueCount,falseCount),
                    isPure:isPure(trueCount,falseCount)
                });
            });

            self.createNode({
                name:feature,
                values:values,
                info:info
            });
            console.log();
        });

        console.log(JSON.stringify(this.nodes, null, 2));

        var bestNode=self.pickBest();

    }

    private isPure (a,b) {
        if((a==0&&b>0)||a>0&&b==0)
            return true;
        return false;
    }
    public predict (){

    }
}

function log2(n) {
    return Math.log(n) / Math.log(2);
}

var tree={
    classifier:"play",
    features:["outlook","humidity","wind"],
    trainingData:[
        { day:1,outlook:"sunny",humidity:"high",wind:"weak",play:false },
        { day:2,outlook:"sunny",humidity:"high",wind:"strong",play:false },
        { day:3,outlook:"overcast",humidity:"high",wind:"weak",play:true },
        { day:4,outlook:"rain",humidity:"high",wind:"weak",play:true },
        { day:5,outlook:"rain",humidity:"normal",wind:"weak",play:true },
        { day:6,outlook:"rain",humidity:"normal",wind:"strong",play:false },
        { day:7,outlook:"overcast",humidity:"normal",wind:"strong",play:true },
        { day:8,outlook:"sunny",humidity:"high",wind:"weak",play:false },
        { day:9,outlook:"sunny",humidity:"normal",wind:"weak",play:true },
        { day:10,outlook:"rain",humidity:"normal",wind:"weak",play:true },
        { day:11,outlook:"sunny",humidity:"normal",wind:"strong",play:true },
        { day:12,outlook:"overcast",humidity:"high",wind:"strong",play:true },
        { day:13,outlook:"overcast",humidity:"normal",wind:"weak",play:true },
        { day:14,outlook:"rain",humidity:"high",wind:"strong",play:false }
    ]
};
var dtree=new DTree(tree);
