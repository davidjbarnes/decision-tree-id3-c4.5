var DecisionTree = require('decision-tree');

var training_data = [
    { day:1,outlook:"sunny",humidity:"high",wind:"weak",play:false },
    { day:2,outlook:"sunny",humidity:"high",wind:"strong",play:false },
    { day:3,outlook:"overcast",humidity:"high",wind:"weak",play:true },
    { day:4,outlook:"rain",humidity:"high",wind:"weak",play:true },
    { day:5,outlook:"rain",humidity:"normal",wind:"weak",play:true },
    { day:6,outlook:"rain",humidity:"normal",wind:"strong",play:false },
    { day:7,outlook:"overcast",humidity:"normal",wind:"strong",play:true },
    { day:8,outlook:"sunny",humidity:"high",wind:"weak",play:false },
    { day:9,outlook:"sunny",humidity:"normal",wind:"weak",play:true },
    { day:10,outlook:"rain",humidity:"normal",wind:"weak",play:true },
    { day:11,outlook:"sunny",humidity:"normal",wind:"strong",play:true },
    { day:12,outlook:"overcast",humidity:"high",wind:"strong",play:true },
    { day:13,outlook:"overcast",humidity:"normal",wind:"weak",play:true },
    { day:14,outlook:"rain",humidity:"high",wind:"strong",play:false }

];

// var test_data = [
//     { day:15,outlook:"rain",humidity:"high",wind:"weak" }
// ];

var class_name = "play";
var features = ["outlook","humidity","wind"];

var dt = new DecisionTree(training_data, class_name, features);

//Predict class label for an instance:
var test_data={ day:15,outlook:"sunny",humidity:"high",wind:"strong" };
var predicted_class = dt.predict(test_data);

//Evaluate model on a dataset:
var accuracy = dt.evaluate(test_data);

//Export underlying model for visualization or inspection:
var treeModel = dt.toJSON();

console.log("predicted_class: "+predicted_class);
console.log("accuracy: "+accuracy);
console.log(treeModel.vals[0]);
console.log(treeModel.vals[0].child.vals[0]);
console.log(treeModel.vals[0].child.vals[1]);